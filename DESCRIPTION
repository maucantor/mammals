Package: MAMMals
Type: Package
Title: Managing Animal Multi Media: Align, Link and Sync
Version: 0.1.0
Date: 2021-01-31
Authors@R: 
  c(person(given = "Alexandre",
           family = "Machado",
           role = "aut",
           email = "alexandre.marcel@posgrad.ufsc.br",
           comment = c(ORCID = "0000-0001-6252-6890")),
    person(given = "Mauricio",
           family = "Cantor",
           role = c("aut", "cre"),
           email = "mcantor@ab.mpg.de",
           comment = c(ORCID = "0000-0002-0019-5106")))
Maintainer: Mauricio Cantor <mcantor@ab.mpg.de>
Description: We introduce the R package ‘MAMMals: Managing Animal MultiMedia: Align, Link, Sync’
    for organizing multimedia data and simplify their processing.
    This package contains functions to (i) link the individual data 
    from photographs to videos, audio recordings and other data sources 
    (such as GPS locations) from which metadata can be accessed; and 
    (ii) synchronize and extract the useful multimedia (e.g. videos with audios) 
    containing photo-identified individuals.
License: GPL
Encoding: UTF-8
LazyData: true
Depends:
    R (>= 3.5.0)
Imports:
  lubridate (>= 1.7.9),
  ggplot2 (>= 3.3.2),
  ggrepel (>= 0.8.2),
  scales (>= 1.1.1),
  stats,
  utils
RoxygenNote: 7.1.1
BugReports: https://bitbucket.org/maucantor/mammals/issues
Suggests: 
    knitr,
    rmarkdown
VignetteBuilder: knitr
