#' @title Calculate horizontal distance of object in kilometers 
#' @description trigonometry to calculate horizontal distance of an object marked in in an image processing software. For example, use the Arrow Tool in imageJ, leaving from a reference point (e.g. camera with GPS) to the object. This distance will be used to transform the marked object into GPS position using the camera as a reference.
#' 
#' @param distance numeric. Distance in meters from the reference point (GPS camera) to the object marked.
#' @param angle numeric. Angle between reference point and marked object.
#' @param km logical. Should the distance be given in kilometers, then km = TRUE. Otherwise it will be given in meters.
#' @note  Make sure that the angle provided has been corrected, first by the side of the video, then by the True North. In the former, if object was marked on the right of the reference point, then angle is the measured one in imageJ; if the object was on the left of the reference point, then angle = (180-angle). In the second, the angle measured in imageJ is relative to the video and must be corrected by the True North via drone flight log data (see \code{doConvertAngle()} and \code{doCorrectCameraYaw}).
#' 
#' @return vertical distance of the object
#' @author Mauricio Cantor

doCalcDistanceX <- function(distance, angle, km = FALSE){

  # calculate distance
  dx <- (cos(angle) * distance)
  
  # transform into KM
  if(isTRUE(km)){
    dx <- dx/1000
    }
  
  return(dy)
}