#' @title Correct camera yaw relative to drone yaw
#' @description If the camera yaw anlge is not relative to the Nort, then the camera's yaw is relative to the drone. Then, to re-project image detection/objects in pixels to latitude and longitude, one should discount camera's yaw (relative to drone) from the drone's yaw (relative to North) and make the difference relative to North. The yaws should to be formated as [0,360] (see \code{doConvertAngle} function).
#' @note When sampling with drones, drone yaw can be anything from 0 to 360 and camera yaw should close to 180 if the camera is pointing straight downwards (when pitch=-90).
#' 
#' @author Mauricio Cantor
#' 
#' @param camera.yaw camera yaw data in degrees
#' @param drone.yaw drone yaw data in degrees
#' @return yaw data in degress [0,360] relative to geopgrahical North
#' 
#' @export

doCorrectCameraYaw <- function(camera.yaw, drone.yaw){
  realyaw = (360 - (correct.yaw(drone.yaw) - correct.yaw(camera.yaw)))
  return(realyaw)
}