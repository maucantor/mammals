#' @title Extracting audio clips containing photo-identified individuals
#' @description This function matches the metadata of audio and the metadata of the photo-identification to output audio clips containing photo-identified individuals
#' @param selected_metadata data frame with metadata of the audio included in the data frame containing the metadata of the photographs produced by the function \code{selectAudios()}.
#' @param output_extension \code{c('.mp3', '.wav')}
#' @param trim_interval Integer. If the chosen output media is a audio clip, \code{output_media='clip'}, this parameter gives the total number of seconds before and after the photo-identification event. The trimmed audio duration will be 2*trim_interval, i.e. \code{duration [-trim_interval, +trim_interval]}. A suggested default is 180 secs (00:03:00) thus exporting a 6-min audio clip.
#' @param timezone string. Add the correct timezone where the audio were recorded, as per the R base notation (for additional information, see \code{help(timezomes)}). Should this argument be left empty, the time zone will be assumed to be the same as the computer system.
#' @param export_audio_folder full path of the destination folder where the audio clips should be saved at 
#' @param export_data_folder full folder path to where the data frame with the matched data should be exported to
#' @param export_data_filename csv name for the export file (e.g. \code{export_data_filename = 'trimmed_audioclip_database.csv'})
#' @param show_ffmpeg_on_console logical. If \code{TRUE}, it prints the FFmpeg output on console. 
#' 
#' @examples \dontrun{
#' getAudioClip(selected_metadata = df_selected_audios, 
#'                                  output_extension = '.wav',
#'                                  timezone = "America/Sao_Paulo", 
#'                                  trim_interval = 60, 
#'                                  export_audio_folder = '/Volumes/HDD/export/sync', 
#'                                  export_data_folder = '/Volumes/HDD/export/sync', 
#'                                  export_data_filename = "audio_clips.csv",
#'                                  show_ffmpeg_on_console = TRUE) }
#' 
#' @note Requires FFmpeg. Please install it from [http://ffmpeg.org]
#' 
#' @author Alexandre Machado, Mauricio Cantor
#' 
#' @importFrom lubridate force_tz as_datetime hms seconds hour ymd_hms with_tz
#' @importFrom utils write.table
#' 
#' @export

getAudioClip <- function(selected_metadata,
                         output_extension = '.wav',
                         timezone = NULL,
                         trim_interval = NULL,
                         export_audio_folder = NULL,
                         export_data_folder = NULL,
                         export_data_filename = NULL,
                         show_ffmpeg_on_console = FALSE) {
  
  # Check arguments
  stopifnot(is.data.frame(selected_metadata))

  # set time zone and force data to it
  if(is.null(timezone)){
    timezone <- Sys.timezone()
    warning(paste("timezone not defined. Using the system timezone:", timezone))
  }
  
  # set trim_interval
  if(is.null(trim_interval))
    stop("Set trim_interval: Number of seconds before and after the photo-identification event.")
  
  if(is.null(export_audio_folder))
    stop("Set export_audio_folder: Full path of the destination folder to save the trimmed audios")
  
  if(is.null(export_data_folder))
    stop("Set export_data_folder: Full path of the destination folder to save a .csv with the data")
  
  if(is.null(export_data_filename))
    stop("Set export_data_filename: Name for the export file 'audio_data.csv' ")
  
  if (isFALSE(grepl(output_extension, ".mp3|.wav", ignore.case = TRUE)))
    stop("Output audio format not supported. Choose between .mp3, .wav")
  
  # selecting the variables needed to trim audio
  metadata <- selected_metadata[, c('camera.photo_filename',
                                    'audio.path', 
                                    'audio.local_start_datetime', 
                                    'audio.duration', 
                                    'audio.photoid_time',
                                    'camera.PHOTOID')]
  
  # force timezone
  metadata$audio.local_start_datetime <- lubridate::force_tz(
    tzone = timezone, roll = FALSE,
    time = lubridate::as_datetime(metadata$audio.local_start_datetime, format = "%Y-%m-%d %H:%M:%S"))
  
  # adding new columns
  metadata$audio_trim_start = NA
  metadata$audio_trim_end = NA
  metadata$audio_local_datetime = as.POSIXct(NA)
  metadata$audio_local_trim_start = as.POSIXct(NA)
  metadata$audio_local_trim_end = as.POSIXct(NA)
  metadata$UTC_datetime_sec = as.POSIXct(NA)
  metadata$audio_UTC_trim_start = as.POSIXct(NA)
  metadata$audio_UTC_trim_end = as.POSIXct(NA)
  metadata$audio_trim_name_out = NA
  
  # create folder to export audio clips
  if(isFALSE(dir.exists(export_audio_folder)))
    dir.create(export_audio_folder)
  
  for(i in 1:nrow(metadata)){ 
    
    # print progress
    cat(paste0(
      "\r", round(100*(i/nrow(metadata)), 1), '% processing, audio file: ', basename(metadata$audio.path[i]),
      " and photo ", i, "/", nrow(metadata), ":", metadata$camera.photo_filename[i]),'\n')
    
    #tmp.file <- paste0(metadata$audio.path[i], metadata$audio_filename[i])
    
    # calculate initial and final time of the trimmed audio, relative to the 
    metadata$audio_trim_start[i] <- secsInDate(lubridate::hms(metadata$audio.photoid_time[i]) - lubridate::seconds(trim_interval))
    metadata$audio_trim_end[i] <- secsInDate(lubridate::hms(metadata$audio.photoid_time[i]) + lubridate::seconds(trim_interval))
    
    # check if interval is smaller than audio length; if so, then the initial time is the start of the original audio
    if (lubridate::hour(lubridate::hms(metadata$audio_trim_start[i])) > 0){
      metadata$audio_trim_start[i] <- as.character("00:00:00")
    }
    
    # check if interval is bigger than audio length; if so, then the audio ends at the duration of the original audio file
    if(isTRUE(lubridate::hms(metadata$audio_trim_end[i]) > lubridate::hms(metadata$audio_duration[i])))
      metadata$audio_trim_end[i] <- metadata$audio.duration[i]
    
    # convert the time of the event (treatment_time) into local datetime by adding number of seconds to audio initial time
    metadata$audio_local_datetime[i] <- as.character(lubridate::ymd_hms(metadata$audio.local_start_datetime[i]) + 
                                                       lubridate::hms(metadata$audio.photoid_time[i]))
    
    # convert initial and final time of trimmed audio to real local datetime
    metadata$audio_local_trim_start[i] <- as.character(lubridate::ymd_hms(metadata$audio.local_start_datetime[i]) + 
                                                         lubridate::hms(metadata$audio_trim_start[i]))
    
    metadata$audio_local_trim_end[i] <- as.character(lubridate::ymd_hms(metadata$audio.local_start_datetime[i]) + 
                                                       lubridate::hms(metadata$audio_trim_end[i]))
    
    # convert all dates to UCT 
    metadata$UTC_datetime_sec[i] <- lubridate::with_tz(metadata$audio_local_datetime[i], tzone = 'UTC')
    metadata$audio_UTC_trim_start[i] <- lubridate::with_tz(metadata$audio_local_trim_start[i], tzone = 'UTC')
    metadata$audio_UTC_trim_end[i] <- lubridate::with_tz(metadata$audio_local_trim_end[i], tzone = 'UTC')
    
    name_out <- gsub(metadata$audio_local_datetime[i], pattern = " ", replacement = "_") 
    name_out <- gsub("[^\\d|\\_]", "", name_out, perl = TRUE)
    
    metadata$audio_trim_name_out[i] <- paste(
      name_out,
      sub('\\..*$', '', metadata$camera.photo_filename[i]),
      metadata$camera.PHOTOID[i],
      sub("(.*\\/)([^.]+)(\\.[[:alnum:]]+$)", "\\2", metadata$audio.path[i]), # filename without extension
      sep = "_")
    
    # Call ffmpeg to clip the original audio
    system(show.output.on.console = show_ffmpeg_on_console, 
           
           paste("ffmpeg -i", 
                 metadata$audio.path[i], 
                 "-ss",
                 metadata$audio_trim_start[i],
                 "-to", 
                 metadata$audio_trim_end[i], 
                 "-c copy",
                 paste0(export_audio_folder, '/', metadata$audio_trim_name_out[i], output_extension),  
                 sep = " ")
    )
    
    # write new create date metadata for audio clips
    system(paste0("exiftool ", 
                  "-createdate=", '\"', as.character(metadata$audio_local_trim_start[i]), '\" ',
                  " -overwrite_original ",
                  paste0(export_audio_folder, '/', metadata$audio_trim_name_out[i], output_extension)))
    
  }
  
  # adding standard UTC time variables
  metadata$UTC_datetime_dec = format(metadata$UTC_datetime_sec, '%Y-%m-%d %H:%M:%OS4')
  metadata$UTC_datetime_min = format(metadata$UTC_datetime_sec, '%Y-%m-%d %H:%M')
  metadata$audio.date = format(metadata$UTC_datetime_sec, '%Y-%m-%d')
  
  
  # organizing output metadata to match other databases
  metadata = metadata[, c("UTC_datetime_dec","UTC_datetime_sec","UTC_datetime_min", 
                          "audio_local_datetime", 
                          "audio.date",
                          "audio.path", 
                          "audio.local_start_datetime", "audio.duration", "audio.photoid_time",
                          "audio_trim_name_out",
                          "audio_trim_start", "audio_trim_end", 
                          "audio_local_trim_start", "audio_local_trim_end",
                          "audio_UTC_trim_start", "audio_UTC_trim_end")]
  
  # rename columns
  names(metadata)[names(metadata) == "audio.local_start_datetime"] <- "audio_original_initial_local_datetime"
  names(metadata)[names(metadata) == "audio.duration"] <- "audio_original_duration"
  names(metadata)[names(metadata) == "audio.photoid_time"] <- "audio_estimated_photoid_time"
  names(metadata)[names(metadata) == "audio_trim_name_out"] <- "audio_trim_filename"
  names(metadata)[names(metadata) == "audio_trim_start"] <- "audio_trim_start_time"
  names(metadata)[names(metadata) == "audio_trim_end"] <- "audio_trim_end_time"
  names(metadata)[names(metadata) == "audio_local_trim_start"] <- "audio_trim_local_initial_datetime"
  names(metadata)[names(metadata) == "audio_local_trim_end"] <- "audio_trim_local_final_datetime"
  names(metadata)[names(metadata) == "audio_UTC_trim_start"] <- "audio_trim_UTC_initial_datetime"
  names(metadata)[names(metadata) == "audio_UTC_trim_end"] <- "audio_trim_UTC_final_datetime"
  
  # Merging selected_metadata with metadata, removing redundancies
  metadata = cbind(subset(selected_metadata, select = - c(audio.path, 
                                                          audio.local_start_datetime,
                                                          audio.duration,
                                                          audio.photoid_time)), 
                   metadata)
  
  # export the metadata to csv?
  if (!is.null(export_data_filename)){ 
    utils::write.table(metadata, file = paste0(export_data_folder, '/', export_data_filename), row.names = FALSE, col.names = TRUE, sep = ",")
  } 
  
  return(metadata)
  
}
