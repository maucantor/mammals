## CORRECT angle TO 360 #####
#' @param data angle in degrees ranging from [0,180] for the right side; and [-180,0] for the left side
#' @return angle in degrees ranging from [0,360]
correctAngle = function(data){
  data[which(data<0)] = 360+(data[which(data<0)])
  return(data)
}




## CORRECT CAMERA YAW RELATIVE TO DRONE YAW #####
# drone yaw can be anything [0,360]
# camera yaw should be as close as 180.00 as possible. This means the camera is pointing straight downwards (when pitch=-90)
# drone and camera yaw coming from the flightlog data are different!
# Using the camera gimbal yaw to re-project the object detection in pixel to lat/long makes more sense IF and only IF the camera yaw angle is relative to North
# IF NOT, then camera's yaw is relative to the drone. This way, we should discount camera's yaw (relative to drone) from the drone's yaw (relative to North) and make the difference relative to North (make sure you correct the yaws to be [0,360] first)
#
#OSD.yaw é refente ao drone... "Degrees. Computed from the Quaternion above. Note, the yaw value appears to be corrected for geomagnetic declination; I.e. yaw is true and not magnetic." Tem mais sobre "Quaternion" aqui: http://datfile.net/DatCon/fields.html. 
#Fico na dúvida se precisa mesmo corrigir para a câmera no realyaw. O gimbal (estabilizador da câmera) rotaciona no próprio eixo (yaw, eixo Z), para cima e para baixo (pitch, eixo Y), e inclina para um lado e para o outro (roll, eixo X). O ângulo desses valores é relativo ao drone. O piloto não tem controle do yaw e do roll, só do pitch (movimento de cima para baixo). Acho que a câmera só rotaciona no eixo Z e X quando o gimbal corrige automaticamente para os movimentos do drone. Nesse exemplo que você mandou, como o GIMBAL.pitch indica que a câmera estava apontada para baixo, não vejo porque corrigir o realyaw pelo gimbal.yaw. Com a câmera olhando pra baixo, a rotação do eixo da câmera não corresponde mais ao eixo do drone.
#
#' @param camera.yaw camera yaw data in degrees
#' @param drone.yaw drone yaw data in degrees
#' @return yaw data in degress [0,360] relative to geopgrahical North
correct.camera.yaw = function(camera.yaw, drone.yaw){
  realyaw = (360 - (correct.yaw(drone.yaw) - correct.yaw(camera.yaw)))
  return(realyaw)
}






## FILTER by DRONE HEIGHT ##########
#' @param height column name of the database with the drone flight height (typically $flight.height)
#' @param threshold scalar. Value in meters to be subtracted from the maxium drone flight height. This will define the range of height that will be kept in the database 
#' @details a permissive rule will be removing everything that was recorded below than 5m from the maximum height of that flight. This rule has to be within videos. You can reduce this height range to make remove more data and be more conservative.
filter.height = function(height, threshold=5){
  # find indices that don't meet the height threshold
  retained.indices = which(height >= (max(height)-threshold) )
  return(retained.indices)
}








## FILTER by CAMERA PITCH/ROLL/YAW ##########
# camera should be looking straight down! roll = 0, yaw = 180, pitch = -90 degrees
# O piloto não tem controle do yaw e do roll da camera, só do pitch (movimento de cima para baixo). Acho que a câmera só rotaciona no eixo Z e X quando o gimbal corrige automaticamente para os movimentos do drone. Nesse exemplo que você mandou, como o GIMBAL.pitch indica que a câmera estava apontada para baixo, não vejo porque corrigir o realyaw pelo gimbal.yaw. Com a câmera olhando pra baixo, a rotação do eixo da câmera não corresponde mais ao eixo do drone.
#
filter.gimbal.pitch = function(camera.pitch, threshold.pitch = 0.1){
  retained.indices.pitch = which(camera.pitch <= (min(camera.pitch)+threshold.pitch) )
    return(retained.indices.pitch)
}
filter.gimbal.roll = function(camera.roll, threshold.roll = 0.1){
  Mode <- function(x) { ux <- unique(x); ux[which.max(tabulate(match(x, ux)))] }
  retained.indices.roll = which(camera.roll <= (Mode(camera.roll)-threshold.roll) & camera.roll >= (Mode(camera.roll)+threshold.roll) )
  return(retained.indices.roll)
}  
filter.gimbal.yaw = function(camera.yaw, threshold.yaw = 5){
  if(sum(camera.yaw < 0) != 0){
    camera.yaw = correct.yaw(camera.yaw)
  }
  retained.indices.yaw =  which(camera.yaw >= (180-threshold.yaw) & camera.yaw <= (180+threshold.yaw)  )
  return(retained.indices.yaw)
}









## FILTER by DRONE PITCH/ROLL/YAW ##########
# drone should be roll = 0, yaw = 180, pitch = 0 degrees
filter.drone.pitch = function(drone.pitch, threshold.pitch = 0.2){
  retained.indices.pitch = which(drone.pitch <= (min(drone.pitch)-threshold.pitch) )
    return(retained.indices.pitch)
}
filter.drone.roll = function(drone.roll, threshold.roll = 0.1){
  Mode <- function(x) { ux <- unique(x); ux[which.max(tabulate(match(x, ux)))] }
  retained.indices.roll = which(drone.roll <= (Mode(drone.roll)-threshold.roll) & drone.roll >= (Mode(drone.roll)+threshold.roll) )
  return(retained.indices.roll)
}  
filter.drone.yaw = function(drone.yaw, threshold.yaw = 5){
  if(sum(drone.yaw < 0) != 0){
    drone.yaw = correct.yaw(drone.yaw)
  }
  retained.indices.yaw =  which(drone.yaw >= (180-threshold.yaw) & drone.yaw <= (180+threshold.yaw)  )
  return(retained.indices.yaw)
}









## FILTER by DRONE SPEED ##########
#' @param speed column name of the database with the drone flight height (typically $flight.height)
#' @param threshold scalar. Value in m/s to be added from the minimum drone flight speed This will define the range of speed that will be kept in the database 
#' @details a permissive rule will be removing everything that was recorded faster than 2m/s. 
filter.speed = function(speed, threshold=2){
  # find indices that don't meet the height threshold
  retained.indices = which(speed <= (min(speed)+threshold) )
  return(retained.indices)
}




## APPLYING FILTER RULES ########################
#' @description wrapper function to apply filters to database
#' @param database data frame to be filtered
#' @param filter.this exact column name of the variable in the database to be filtered
#' @param by.this string with exact column name of the variable in the database (OR a string vector with more than one column) to serve as a factor to tapply the function
#' @param filter.function custom function to apply
apply.filter = function(database=data_drone_flight, filter.this='column-name', by.this=c('drone.folder', 'drone.video_file_name'), filter.function, ...){
  if(is.null(by.this)){
    filtered.indices = filter.function(database[, filter.this], ...)
    filtered = database[filtered.indices, ]
    #filtered = sapply(database[, filter.this], FUN = filter.function)
    #filtered = do.call("rbind", filtered)
    return(filtered)
    
  } else { # by.this!=NULL, function must be applied within a given factor
    # make factor to tapply
    if(length(by.this)==2){ 
      by.this = paste0(database[,by.this[1]], '/', database[,by.this[2]])
    } else {
      by.this = database[, by.this]
    }
    # temporarily add by.this as factor to the database
    as.factor(by.this)
    database$by.this = by.this
    
    # get the indices of the rows that passed the rule, that is, that were not filtered off
    filtered.indices = tapply(database[,filter.this], by.this, FUN=filter.function, ...)
    
    # create list of lists to receive the indices per video
    #filtered = database[as.numeric(unlist(filtered.indices)), ] # doesnt work: mix up the indices
    auxlist = rep( list(list()), length(filtered.indices) ) 
    for(i in 1:length(filtered.indices)){
      auxdatabase = database[which(database$by.this==names(filtered.indices)[i]), ]
      auxlist[[i]] = auxdatabase[filtered.indices[[i]], ]
    }
    # merge them
    filtered = do.call("rbind", auxlist)
    # remove factor
    filtered$by.this = NULL
    return(filtered)
  }
}




# time char to seconds
timechar2sec = function(x){
  return(as.numeric(strsplit(x, ":")[[1]]) %*% c(3600, 60, 1))
}

# rescale from 0-600 seconds
rescale <- function(x, r.out) {
  p <- (x - min(x)) / (max(x) - min(x))
  r.out[[1]] + p * (r.out[[2]] - r.out[[1]])
}





##### Calculate horizontal distance of object in kilometers #####

#' @description trigonometry to calculate horizontal distance of an object (dolphin, fisher) marked in imageJ with the Arrow Tool leaving from the camera with GPS. This distance will be used to transform the marked object into GPS position using the camera as a reference.
#' @param distance hypothenuse: distance, in meters, of reference point (GPS camera) and the object marked (lenght, in meters)
#' @param angle angle between reference point and marked object.
#' @param km Boolean. Should the distance be given in kilometers, then km=TRUE. Otherwise it will be given in meters
#' @note  Make sure that the angle provided has been corrected, first by the side of the video, then by the True North. In the former, if object was marked on the right (R) to the camera, then angle is the measured one in imageJ; if the object was on the left (L) to the camera, then angle = 180-angle). In the second, the angle measured in imageJ is relative to the video and must be corrected by the True North (via drone flight log data), so this angle becomes relative to the canal (where, 0=inward, 180=outward, 270=towards fisher line, 90=away fisher line)
#' @return vertical distance of the object
#' @author Mauricio Cantor, mcantor@ab.mpg.de

distance_x = function(distance, angle, km=FALSE){
  # transform angle to radians
  #angle = angle*(pi/180)
  # calculate distance
  dx = (cos(angle) * distance)
  # transform into KM
  if(km==TRUE){dx = dx/1000}
  return(dx)
}




##### Calculate vertical distance of object in kilometers #####

#' @description trigonometry to calculate vertical distance of an object (dolphin, fisher) marked in imageJ with the Arrow Tool leaving from the camera with GPS. This distance will be used to transform the marked object into GPS position using the camera as a reference.
#' @param distance hypothenuse: distance, in meters, of reference point (GPS camera) and the object marked (lenght)
#' @param angle angle between reference point and marked object.
#' @param km Boolean. Should the distance be given in kilometers, then km=TRUE. Otherwise it will be given in meters
#' @note  Make sure that the angle provided has been corrected, first by the side of the video, then by the True North. In the former, if object was marked on the right (R) to the camera, then angle is the measured one in imageJ; if the object was on the left (L) to the camera, then angle = 180-angle). In the second, the angle measured in imageJ is relative to the video and must be corrected by the True North (via drone flight log data), so this angle becomes relative to the canal (where, 0=inward, 180=outward, 270=towards fisher line, 90=away fisher line)
#' @return vertical distance of the object
#' @author Mauricio Cantor, mcantor@ab.mpg.de

distance_y = function(distance, angle, km=FALSE){
  # transform angle to radians
  #angle = angle*(pi/180)
  # calculate distance
  dy = (sin(angle) * distance)
  # transform into km
  if(km==TRUE){ dy = dy/1000}
  return(dy)
}








##### Calculate radius of Earth at given latitude and altitude ##### 

#' @description The Earth radius at sea level is 6371.001 km on average, but it will change with the latitude. Since Laguna is in the subtropics, and we are measuring fine-scale distances, we will correct it by using the mean values of latitude (camera.latitude) and altitude (camera.masl) of camera GPS for all photos taken during the analysed video 
#' @param lat latitude in decimal degrees. It will be converted into radians in the calculus
#' @param masl meters above sea level
#' @note longitude is not needed here if we assume that, mathematically, the Earth is a spheroid (& certainly not flat! :)
#' @return earth radius
#' @author Mauricio Cantor, mcantor@ab.mpg.de

radius_earth = function(lat, masl=0){
  # r1: radius at equator at sea level
  r1= 6378.137
  # r2: radius at pole at sea level
  r2= 6356.7523142
  
  # convert the given latitude to radians
  lat = lat*(pi/180)
  
  # Earth radius
  R = sqrt( ((r1^2 * cos(lat))^2 + (r2^2 * sin(lat))^2)  /  ((r1 * cos(lat))^2   + (r2 * sin(lat))^2)  )
  R = R + (masl/1000)
  return(R)
}








##### Calculate latitude of an object given the distance and angle to a reference point ##### 

#' @description Calculate the latitude of an object (dolphin, fisher) marked in imageJ with the Arrow Tool leaving from a reference point (the camera with GPS). This conversion will assume that:
# # The number of kilometers per degree of longitude is approximately:
# (2*pi/360) * radius_earth(lat = 0, masl = 0) * cos(mean_lat_laguna) # 108.6016 km /degree longitude
# # The number of kilometers per degree of latitude is approximately the same for all locations:
# (2*pi/360) * radius_earth(lat = 0, masl = 0) # 111.3195 km /degree latitude

#' @param distance_vertical vertical distance of the object to the reference (GPS camera). This can be the output of /code{distance_y()}
#' @param latitude_reference latitude in decimal degrees of the reference object. Recommended using the average latitude of the camera with GPS during a given video, but we will set the default value to be the average latitude of all 10000 photos taken in 2019 (mean_lat_laguna = -28.49576)
#' @param masl meters above sea level of the reference object. Recommended using the average altitude of the camera with GPS during a given video, but we will set the default value to be the average altitude of all 10000 photos taken in 2019 (mean_masl_laguna = 0.01708348)
#' @param km Boolean. Is the distances be  in kilometers (km=TRUE) or meters (km=FALSE)
#' @return latitude of the object
#' @author Mauricio Cantor, mcantor@ab.mpg.de

new_latitude = function(distance_vertical, latitude_reference = (-28.49576), masl = 0.01708348, km=FALSE){
  
  # earth radius
  earth_radius = radius_earth(lat = latitude_reference, masl = masl) * 1000
  if(km==TRUE){ earth_radius = earth_radius }
  
  # formula: new_latitude = latitude_reference + (distance_vertical / earth_radius) * (180/pi)
  new_latitude = latitude_reference + (distance_vertical / earth_radius) * (180/pi)
  
  return(new_latitude)
}






##### Calculate longitude of an object given the distance and angle to a reference point ##### 
#' @description Calculate the longitude of an object (dolphin, fisher) marked in imageJ with the Arrow Tool leaving from a reference point (the camera with GPS). This conversion will assume that:
# # The number of kilometers per degree of longitude is approximately:
# (2*pi/360) * radius_earth(lat = 0, masl = 0) * cos(mean_lat_laguna) # 108.6016 km /degree longitude
# # The number of kilometers per degree of latitude is approximately the same for all locations:
# (2*pi/360) * radius_earth(lat = 0, masl = 0) # 111.3195 km /degree latitude

#' @param distance_horizontal horizontal distance of the object to the reference (GPS camera). This can be the output of /code{distance_x()}
#' @param latitude_reference latitude in decimal degrees of the reference object. Recommended using the average latitude of the camera with GPS during a given video, but we will set the default value to be the average latitude of all 10000 photos taken in 2019 (mean_lat_laguna = -28.49576)
#' @param longitude_reference longitude in decimal degrees of the reference object. Recommended using the average longitude of the camera with GPS during a given video, but we will set the default value to be the average longitude of all 10000 photos taken in 2019 (mean_lat_laguna = -48.75996)
#' @param masl meters above sea level of the reference object. Recommended using the average altitude of the camera with GPS during a given video, but we will set the default value to be the average altitude of all 10000 photos taken in 2019 (mean_masl_laguna = 0.01708348)
#' @param km Boolean. Is the distances be  in kilometers (km=TRUE) or meters (km=FALSE)
#' @return longitude of the object
#' @author Mauricio Cantor, mcantor@ab.mpg.de

new_longitude = function(distance_horizontal, latitude_reference = -28.49576, longitude_reference = -28.49576, masl = 0.01708348, km=FALSE){
  
  # earth radius
  earth_radius = radius_earth(lat = latitude_reference, masl = masl) * 1000
  if(km==TRUE){ earth_radius = earth_radius }
  
  # formula: new_longitude = longitude_reference + (distance_horizontal / earth_radius) * ((180/pi)/cos(latitude_reference * (pi/180)))
  new_longitude = longitude_reference + (distance_horizontal / earth_radius) * ((180/pi)/cos(latitude_reference * (pi/180)))
  
  return(new_longitude)
}




## RESCALE DISTRIBUTION ########################################

# Rescale
#' @title Rescaling distribution
#' @description Rescales a distribution to a chosen range
#' @param x vector to be rescaled
#' @param r.out vector with limits of the new distribution
#' @return vector with rescaled distribution
#' @examples
#' # generating 10-sample uniform distribution from 1 to 100 and rescaling it to 0 to 1
#' data <- runif(10, 1, 100)
#' rescale(data, r.out=c(0,1))
rescale <- function(x, r.out) {
  p <- (x - min(x)) / (max(x) - min(x))
  r.out[[1]] + p * (r.out[[2]] - r.out[[1]])
}

