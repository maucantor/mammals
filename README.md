# The MAMMals R package: Managing Animal Multi Media: Align, Link and Sync

[![Documentation](https://img.shields.io/badge/documentation-MAMMals-orange.svg?colorB=E00000)](https://mammals-rpackage.netlify.app) [![experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#maturing)


Identifying individual animals is critical to describe demographic and behavioural patterns, and to investigate the ecological and evolutionary underpinnings of these patterns. The traditional non-invasive method of individual identification in mammals—comparison of photographed natural marks—has been improved by coupling other sampling methods, such as recording overhead video, audio and other multimedia data. However, aligning, linking and syncing these multimedia data streams are persistent challenges. Here, we provide computational tools to streamline the integration of multiple techniques to identify individual free-ranging mammals when tracking their behaviour in the wild. We developed an open-source R package for organizing multimedia data and for simplifying their processing a posteriori—“MAMMals: Managing Animal MultiMedia: Align, Link, Sync”. The package contains functions to (i) align and link the individual data from photographs to videos, audio recordings and other text data sources (e.g. GPS locations) from which metadata can be accessed; and (ii) synchronize and extract the useful multimedia (e.g. videos with audios) containing photo-identified individuals. To illustrate how these tools can facilitate linking photo-identification and video behavioural sampling in situ, we simultaneously collected photos and videos of bottlenose dolphins using off-the-shelf cameras and drones, then merged these data to track the foraging behaviour of individuals and groups. We hope our simple tools encourage future work that extend and generalize the links between multiple sampling platforms of free-ranging mammals, thereby improving the raw material needed for generating new insights in mammalian population and behavioural ecology.

------------------------------------------------

# Installation and dependencies

The MAMMals R package runs on the [R environment](https://cran.r-project.org) and depends on two external software:  

### Exiftool 
[ExifTool](https://exiftool.org) is used to extract the metadata of media files. Instructions to download and install the ExifTool are available here: https://exiftool.org/install.html

### FFmpeg
[FFmpeg](https://ffmpeg.org) is used to clip video and audio files. Instructions to download and install the FFmpeg are available here: https://www.ffmpeg.org/download.html


### Install MAMMals R Package

The `MAMMals` package can be installed from the [Bitbucket repository](https://bitbucket.org/maucantor/mammals/src/master/): 

##### 1. Launch R 

##### 2. Install and/or load the package `remotes`: 

```
if(!require(remotes)){install.packages('remotes'); library(remotes)} 
```

##### 3. Install and load the package `MAMMals`: 

```
remotes::install_bitbucket(repo = 'maucantor/mammals')
```

##### 4. Load `MAMMals`

```
library(MAMMals)
```

### Online tutorial 

See the [MAMMals R Package website](https://mammals-rpackage.netlify.app) for a tutorial, a [list of all functions](https://mammals-rpackage.netlify.app/reference/index.html), and a step-by-step guide on:     
- [Extracting metadata of multimedia files](https://mammals-rpackage.netlify.app/articles/extracting_metadata.html)     
- [Aligning multimedia files](https://mammals-rpackage.netlify.app/articles/aligning.html)     
- [Linking photographs with multimedia data](https://mammals-rpackage.netlify.app/articles/linking.html)     
- [Syncing multimedia data](https://mammals-rpackage.netlify.app/articles/syncing.html)     

------------------------------------------------

# Collaborative Development

These tools can streamline the integration of photographic techniques to identify individual animals with other multiple multimedia sampling platforms. However, we acknowledge there is room for improvement. We encourage further development of these tools collectively by making all the code entirely open and ready to be edited. If you would like to contribute to the further development of these tools, feel free to [fork the repository](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/). Contributors may need to set a [free Bitbucket account](https://www.bitbucket.org). To become a formal collaborator, please email us and we will grant you reading, writing and administrative access to the repository.

## Got a bug or suggestion? 

If you have found a bug, have any suggestions to improve the existing functions or want to propose a new feature, please file an issue at https://bitbucket.org/maucantor/mammals/issues.

------------------------------------------------

# Maintainers and Contact

**Alexandre MS Machado**

* Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Brazil

* Email: alexandremarcelsm at hotmail.com

**Mauricio Cantor**

* Department for the Ecology of Animal Societies, Max Planck Institute of Animal Behavior, Germany.

* Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Brazil

* Email: mcantor at ab.mpg.de


------------------------------------------------

# Reference

This is a supplementary material of the article [Machado & Cantor (2021)](https://link.springer.com/article/10.1007/s42991-021-00189-0) published in Mammalian Biology.

If you use some of these tools, please cite: 

```
Machado, A.M.S., Cantor, M. 2021. A simple tool for linking photo-identification with multimedia data to track mammal behaviour. 
Mamm Biol 102, 983–993. doi: https://doi.org/10.1007/s42991-021-00189-0
```



------------------------------------------------

